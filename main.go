package main

import (
	"flag"
	"fmt"
	"gitlab.com/jvdnboe/stringutil"
	"gitlab.com/jvdnboe/stringutil/ignorecase"
	"os"
)

var searchKey string
var caseInsensitive bool

func init() {
	flag.StringVar(&searchKey, "search-key", "foobar", "key to search")
	flag.BoolVar(&caseInsensitive, "ignore-case",  false, "ignore case")
}

func main() {
	flag.Parse()

	var containsFunc func(string, []string) bool
	if caseInsensitive {
		containsFunc = ignorecase.ContainsString
	} else {
		containsFunc = stringutil.ContainsString
	}

	if contains := containsFunc(searchKey, flag.Args()); contains {
		fmt.Println("It sure contains it!")
		os.Exit(0)
	}

	os.Exit(1)
}
